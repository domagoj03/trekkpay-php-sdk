<?php

namespace TrekkPay\Sdk\ApiClient\Exceptions;

class InvalidArgumentException extends Exception
{
}
