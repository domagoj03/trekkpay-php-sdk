<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Http\Response;

final class PaymentPage extends MethodsCollection
{
    /**
     * @param string $paymentPageId
     * @param array  $params
     *
     * @return Response
     */
    public function getDetails($paymentPageId, array $params = [])
    {
        $params['payment_page_id'] = $paymentPageId;

        return $this->request('paymentPage.getDetails', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     */
    public function initialize(array $params)
    {
        return $this->request('paymentPage.initialize', $params);
    }
}
