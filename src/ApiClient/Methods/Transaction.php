<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Http\Response;

final class Transaction extends MethodsCollection
{
    /**
     * @param string $transactionId
     * @param array  $params
     *
     * @return Response
     */
    public function getDetails($transactionId, array $params = [])
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.getDetails', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     */
    public function getDccQuote(array $params)
    {
        return $this->request('transaction.getDccQuote', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     */
    public function initialize(array $params)
    {
        return $this->request('transaction.initialize', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     */
    public function authorize(array $params)
    {
        return $this->request('transaction.authorize', $params);
    }

    /**
     * @param string $transactionId
     * @param array  $params
     *
     * @return Response
     */
    public function capture($transactionId, array $params = [])
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.capture', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     */
    public function pay(array $params)
    {
        return $this->request('transaction.pay', $params);
    }

    /**
     * @param string $transactionId
     * @param array  $params
     *
     * @return Response
     */
    public function refund($transactionId, array $params = [])
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.refund', $params);
    }

    /**
     * @param string $transactionId
     * @param array  $params
     *
     * @return Response
     */
    public function reverse($transactionId, array $params = [])
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.reverse', $params);
    }

    /**
     * @param string $transactionId
     *
     * @return Response
     */
    public function void($transactionId)
    {
        return $this->request('transaction.void', ['transaction_id' => $transactionId]);
    }

    /**
     * @param string $transactionId
     *
     * @return Response
     */
    public function abort($transactionId)
    {
        return $this->request('transaction.abort', ['transaction_id' => $transactionId]);
    }
}
