<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Http\Response;

final class Accounting extends MethodsCollection
{
    /**
     * @param int   $merchantId
     * @param array $params
     *
     * @return Response
     */
    public function listAccountsForMerchant($merchantId, array $params = [])
    {
        $params['merchant_id'] = (int) $merchantId;

        return $this->request('journal.listAccountsForMerchant', $params);
    }

    /**
     * @param int   $merchantId
     * @param array $params
     *
     * @return Response
     */
    public function listPayoutStatementsForMerchant($merchantId, array $params = [])
    {
        $params['merchant_ids'] = [(int) $merchantId];

        return $this->request('payoutStatements.search', $params);
    }
}
